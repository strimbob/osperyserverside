"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var media = {
  phone: 414,
  phone_both: 414,
  tablet: 768,
  desktop: 920
};
exports.default = media;