'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _configFireBase = require('./Components/firebase/configFireBase.js');

var _configFireBase2 = _interopRequireDefault(_configFireBase);

var _offer = require('./Components/subComps/offer.js');

var _offer2 = _interopRequireDefault(_offer);

var _menuBar = require('./Components/subComps/menuBar.js');

var _menuBar2 = _interopRequireDefault(_menuBar);

var _splat = require('./Components/subComps/splat.js');

var _splat2 = _interopRequireDefault(_splat);

var _offerSelector = require('./Components/subComps/offerSelector.js');

var _offerSelector2 = _interopRequireDefault(_offerSelector);

var _offerPageTypeTwo = require('./Components/offerPageTypeTwo.js');

var _offerPageTypeTwo2 = _interopRequireDefault(_offerPageTypeTwo);

var _offerPage = require('./Components/offerPage.js');

var _offerPage2 = _interopRequireDefault(_offerPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Contentsmainpage(props) {

  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(_offerPage2.default, null)
  );
}
exports.default = Contentsmainpage;