'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactRouterDom = require('react-router-dom');

var _App = require('./App');

var _App2 = _interopRequireDefault(_App);

var _OfferPage = require('./Components/OfferPage.js');

var _OfferPage2 = _interopRequireDefault(_OfferPage);

var _offerPageTypeTwo = require('./Components/offerPageTypeTwo.js');

var _offerPageTypeTwo2 = _interopRequireDefault(_offerPageTypeTwo);

var _configFireBase = require('./Components/firebase/configFireBase.js');

var _configFireBase2 = _interopRequireDefault(_configFireBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('./scss/screen.scss');

(0, _reactDom.render)(_react2.default.createElement(
    _reactRouterDom.BrowserRouter,
    { basename: process.env.PUBLIC_URL },
    _react2.default.createElement(
        _reactRouterDom.Switch,
        null,
        _react2.default.createElement(_reactRouterDom.Route, { exact: true, path: '/', component: _App2.default }),
        _react2.default.createElement(_reactRouterDom.Route, { path: '/someTile', render: function render(props) {
                return _react2.default.createElement(_OfferPage2.default, props);
            } }),
        _react2.default.createElement(_reactRouterDom.Route, { path: '/someTileTwo', render: function render(props) {
                return _react2.default.createElement(_offerPageTypeTwo2.default, props);
            } }),
        _react2.default.createElement(_reactRouterDom.Route, { path: '/offers', render: function render(props) {
                return _react2.default.createElement(_OfferPage2.default, props);
            } }),
        _react2.default.createElement(_reactRouterDom.Route, { exact: true, path: '/workplease', render: function render(props) {
                return _react2.default.createElement(_OfferPage2.default, props);
            } })
    )
), document.querySelector('#root'));

// render(<App />, document.querySelector('#root'));