'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _offer = require('./Components/subComps/offer.js');

var _offer2 = _interopRequireDefault(_offer);

var _menuBar = require('./Components/subComps/menuBar.js');

var _menuBar2 = _interopRequireDefault(_menuBar);

var _splat = require('./Components/subComps/splat.js');

var _splat2 = _interopRequireDefault(_splat);

var _footer = require('./Components/subComps/footer.js');

var _footer2 = _interopRequireDefault(_footer);

var _offerSelector = require('./Components/subComps/offerSelector.js');

var _offerSelector2 = _interopRequireDefault(_offerSelector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function App(props) {

  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(_menuBar2.default, null),
    _react2.default.createElement(_splat2.default, null),
    _react2.default.createElement(_offerSelector2.default, null),
    _react2.default.createElement(_footer2.default, null)
  );
}
exports.default = App;