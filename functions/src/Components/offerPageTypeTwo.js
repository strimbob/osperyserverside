'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _offer = require('./subComps/offer.js');

var _offer2 = _interopRequireDefault(_offer);

var _menuBar = require('./subComps/menuBar.js');

var _menuBar2 = _interopRequireDefault(_menuBar);

var _splat = require('./subComps/splat.js');

var _splat2 = _interopRequireDefault(_splat);

var _offerSelector = require('./subComps/offerSelector.js');

var _offerSelector2 = _interopRequireDefault(_offerSelector);

var _shareButtons = require('./subComps/shareButtons.js');

var _shareButtons2 = _interopRequireDefault(_shareButtons);

var _fireBaseReqest = require('./firebase/fireBaseReqest.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var OfferPageTypeTwo = function (_React$Component) {
  _inherits(OfferPageTypeTwo, _React$Component);

  function OfferPageTypeTwo(props) {
    _classCallCheck(this, OfferPageTypeTwo);

    var _this = _possibleConstructorReturn(this, (OfferPageTypeTwo.__proto__ || Object.getPrototypeOf(OfferPageTypeTwo)).call(this, props));

    _this.state = { storyArray: [] };
    return _this;
  }

  _createClass(OfferPageTypeTwo, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _this2 = this;

      (0, _fireBaseReqest.getStoryFireBase)('offers').then(function (_story) {
        var allStory = [];
        for (var q = 0; q < 3; q++) {
          console.log(_story[Object.keys(_story)[q]]);
          allStory.push(_story[Object.keys(_story)[q]]);
        }
        _this2.setState({ storyArray: allStory });
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {}
  }, {
    key: 'render',
    value: function render() {

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_menuBar2.default, null),
        _react2.default.createElement(
          'div',
          { className: 'content' },
          _react2.default.createElement(
            'div',
            { className: 'content__box' },
            _react2.default.createElement(
              'div',
              { className: 'content__title' },
              '360\u02DA EXPERIENCE'
            ),
            _react2.default.createElement('div', { className: 'content__line' }),
            _react2.default.createElement('div', { className: 'content__space' }),
            _react2.default.createElement('img', { className: 'content__Image', src: 'http://lorempixel.com/250/250/sports/', alt: '/|\\' }),
            _react2.default.createElement(
              'div',
              { className: 'content__body' },
              '360\u02DA experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods\u2026 you get the picture\u2026 experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360\u02DA experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods\u2026 you get the picture\u2026 experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360\u02DA experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods\u2026 you get the picture\u2026 experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360\u02DA experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods\u2026 you get the picture\u2026 experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360\u02DA experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods\u2026 you get the picture\u2026 experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas'
            ),
            _react2.default.createElement('div', { className: 'content__line' }),
            _react2.default.createElement(
              'div',
              { className: 'content__shareButtons' },
              _react2.default.createElement(_shareButtons2.default, null)
            ),
            _react2.default.createElement('div', { className: 'content__line' }),
            _react2.default.createElement(
              'div',
              { className: 'content__title content__title--sub' },
              'other work'
            ),
            _react2.default.createElement(
              'div',
              { className: 'content__subOffer' },
              this.state.storyArray.map(function (storyArray, index) {
                return _react2.default.createElement(_offer2.default, { story: storyArray, key: index });
              })
            )
          )
        )
      );
    }
  }]);

  return OfferPageTypeTwo;
}(_react2.default.Component);

// require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicator');
// import  addIndicators from 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
// import ScrollMagic from 'scrollmagic';
// import { TweenMax } from 'gsap';
// import * as ScrollMagic from 'ScrollMagic';
// import "ScrollMagic/scrollmagic/minified/plugins/debug.addIndicators.min.js";
// import 'ScrollMagic/scrollmagic/minified/plugins/animation.gsap.min.js';


exports.default = OfferPageTypeTwo;