'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactResponsive = require('react-responsive');

var _reactResponsive2 = _interopRequireDefault(_reactResponsive);

var _mediaRect = require('../../scss/mediaRect.js');

var _mediaRect2 = _interopRequireDefault(_mediaRect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ImgDestop;
var ImgPhone;

var MediaQueriesImgIndex = function (_React$Component) {
  _inherits(MediaQueriesImgIndex, _React$Component);

  function MediaQueriesImgIndex(props) {
    _classCallCheck(this, MediaQueriesImgIndex);

    var _this = _possibleConstructorReturn(this, (MediaQueriesImgIndex.__proto__ || Object.getPrototypeOf(MediaQueriesImgIndex)).call(this, props));

    _this.state = { ImgDestop: _this.props.story.imgDtop,
      ImgPhone: _this.props.story.imgPhone
    };
    return _this;
  }

  _createClass(MediaQueriesImgIndex, [{
    key: 'componentDidMount',
    value: function componentDidMount() {

      if (this.props.story.indexDisplay == 'big') {
        this.setState({
          ImgDestop: this.props.story.imgDtop,
          ImgPhone: this.props.story.imgPhone
        });
        // this.setState({
        // ImgDestop : "http://via.placeholder.com/400x230",
        // ImgPhone : "http://via.placeholder.com/400x230"
        // })

      } else if (this.props.story.indexDisplay == 'small') {
        this.setState({
          ImgDestop: this.props.story.imgDtopS,
          ImgPhone: this.props.story.imgPhoneS
        });

        //  this.setState({
        //   ImgDestop : "http://via.placeholder.com/217x126",
        //   ImgPhone  : "http://via.placeholder.com/269x400"
        // })
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactResponsive2.default,
          { minWidth: _mediaRect2.default.desktop + 1 },
          _react2.default.createElement('img', { src: this.state.ImgDestop, alt: '\\|/' })
        ),
        _react2.default.createElement(
          _reactResponsive2.default,
          { maxWidth: _mediaRect2.default.desktop },
          _react2.default.createElement('img', { src: this.state.ImgPhone, alt: '\\|/' })
        )
      );
    }
  }]);

  return MediaQueriesImgIndex;
}(_react2.default.Component);

exports.default = MediaQueriesImgIndex;