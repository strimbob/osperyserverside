'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactShare = require('react-share');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FacebookShareButton = _reactShare.ShareButtons.FacebookShareButton,
    GooglePlusShareButton = _reactShare.ShareButtons.GooglePlusShareButton,
    LinkedinShareButton = _reactShare.ShareButtons.LinkedinShareButton,
    TwitterShareButton = _reactShare.ShareButtons.TwitterShareButton,
    PinterestShareButton = _reactShare.ShareButtons.PinterestShareButton,
    VKShareButton = _reactShare.ShareButtons.VKShareButton,
    OKShareButton = _reactShare.ShareButtons.OKShareButton,
    TelegramShareButton = _reactShare.ShareButtons.TelegramShareButton,
    WhatsappShareButton = _reactShare.ShareButtons.WhatsappShareButton,
    RedditShareButton = _reactShare.ShareButtons.RedditShareButton,
    EmailShareButton = _reactShare.ShareButtons.EmailShareButton;
var FacebookShareCount = _reactShare.ShareCounts.FacebookShareCount,
    GooglePlusShareCount = _reactShare.ShareCounts.GooglePlusShareCount,
    LinkedinShareCount = _reactShare.ShareCounts.LinkedinShareCount,
    PinterestShareCount = _reactShare.ShareCounts.PinterestShareCount,
    VKShareCount = _reactShare.ShareCounts.VKShareCount,
    OKShareCount = _reactShare.ShareCounts.OKShareCount,
    RedditShareCount = _reactShare.ShareCounts.RedditShareCount;


var FacebookIcon = (0, _reactShare.generateShareIcon)('facebook');
var TwitterIcon = (0, _reactShare.generateShareIcon)('twitter');
var GooglePlusIcon = (0, _reactShare.generateShareIcon)('google');
var LinkedinIcon = (0, _reactShare.generateShareIcon)('linkedin');
var PinterestIcon = (0, _reactShare.generateShareIcon)('pinterest');
var VKIcon = (0, _reactShare.generateShareIcon)('vk');
var OKIcon = (0, _reactShare.generateShareIcon)('ok');
var TelegramIcon = (0, _reactShare.generateShareIcon)('telegram');
var WhatsappIcon = (0, _reactShare.generateShareIcon)('whatsapp');
var RedditIcon = (0, _reactShare.generateShareIcon)('reddit');
var EmailIcon = (0, _reactShare.generateShareIcon)('email');

var Share = function (_React$Component) {
  _inherits(Share, _React$Component);

  function Share() {
    _classCallCheck(this, Share);

    return _possibleConstructorReturn(this, (Share.__proto__ || Object.getPrototypeOf(Share)).apply(this, arguments));
  }

  _createClass(Share, [{
    key: 'render',
    value: function render() {

      var dbAddress = this.props.data.location.pathname;
      var dbSearch = this.props.data.location.search;

      // let dbAddress = "this.props.data.location.pathname";
      // let dbSearch = "this.props.data.location.search";

      console.log("dbAddress+dbSearch");
      console.log(dbAddress + dbSearch);
      var address = 'http://osprey.co.uk' + dbAddress + dbSearch;
      console.log(this.props.data);
      var shareUrl = address;
      var title = this.props.title;

      // const shareUrl = 'sfsdfs';
      // const title =  'gdsf';

      return _react2.default.createElement(
        'div',
        { className: 'Demo__container' },
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            FacebookShareButton,
            {
              url: shareUrl,
              quote: title,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(FacebookIcon, {
              size: 32,
              round: true })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            TwitterShareButton,
            {
              url: shareUrl,
              title: title,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(TwitterIcon, {
              size: 32,
              round: true })
          ),
          _react2.default.createElement(
            'div',
            { className: 'Demo__some-network__share-count' },
            '\xA0'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            TelegramShareButton,
            {
              url: shareUrl,
              title: title,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(TelegramIcon, { size: 32, round: true })
          ),
          _react2.default.createElement(
            'div',
            { className: 'Demo__some-network__share-count' },
            '\xA0'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            WhatsappShareButton,
            {
              url: shareUrl,
              title: title,
              separator: ':: ',
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(WhatsappIcon, { size: 32, round: true })
          ),
          _react2.default.createElement(
            'div',
            { className: 'Demo__some-network__share-count' },
            '\xA0'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            GooglePlusShareButton,
            {
              url: shareUrl,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(GooglePlusIcon, {
              size: 32,
              round: true })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            LinkedinShareButton,
            {
              url: shareUrl,
              title: title,
              windowWidth: 750,
              windowHeight: 600,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(LinkedinIcon, {
              size: 32,
              round: true })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            RedditShareButton,
            {
              url: shareUrl,
              title: title,
              windowWidth: 660,
              windowHeight: 460,
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(RedditIcon, {
              size: 32,
              round: true })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'Demo__some-network' },
          _react2.default.createElement(
            EmailShareButton,
            {
              url: shareUrl,
              subject: title,
              body: 'body',
              className: 'Demo__some-network__share-button' },
            _react2.default.createElement(EmailIcon, {
              size: 32,
              round: true })
          )
        )
      );
    }
  }]);

  return Share;
}(_react2.default.Component);

exports.default = Share;