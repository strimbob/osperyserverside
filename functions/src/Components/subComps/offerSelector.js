'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _offer = require('./offer.js');

var _offer2 = _interopRequireDefault(_offer);

var _divder = require('./divder.js');

var _divder2 = _interopRequireDefault(_divder);

var _fireBaseReqest = require('../firebase/fireBaseReqest.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counterL = 0;
var counterR = 0;
var switcherL = 0;
var switcherR = 1;

var OfferSelector = function (_React$Component) {
  _inherits(OfferSelector, _React$Component);

  function OfferSelector(props) {
    _classCallCheck(this, OfferSelector);

    var _this = _possibleConstructorReturn(this, (OfferSelector.__proto__ || Object.getPrototypeOf(OfferSelector)).call(this, props));

    _this.state = {
      storyArray: []
    };
    return _this;
  }

  _createClass(OfferSelector, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _this2 = this;

      var lic = require('../../../public/static/offer/360˚ EXPERIENCE.js');
      console.log(lic);
      (0, _fireBaseReqest.getStoryFireBase)('offers').then(function (_story) {
        var allStory = [];
        for (var q = 0; q < Object.keys(_story).length; q++) {
          allStory.push(_story[Object.keys(_story)[q]]);
          allStory[q].length = Object.keys(_story).length;
        }
        _this2.setState({ storyArray: allStory });
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'offerSelector' },
          ' ',
          this.state.storyArray.map(function (storyArray, index) {
            addLeftRight(index);
            function addLeftRight(index) {
              var whichSide = index & 1 ? 'right' : 'left';
              storyArray.addedClass = "offer__" + whichSide;
              if (whichSide == 'left') {
                bigSmall(switcherL);
                switcherL = !switcherL;
              } else {
                bigSmall(switcherR);
                switcherR = !switcherR;
              }
            }
            function bigSmall(_switcher) {
              if (_switcher) storyArray.indexDisplay = 'big';else storyArray.indexDisplay = 'small';
              storyArray.addedClass += ' ' + 'offer__' + storyArray.indexDisplay;
            }
            return _react2.default.createElement(_offer2.default, { story: storyArray, key: index });
          })
        )
      );
    }
  }]);

  return OfferSelector;
}(_react2.default.Component);

exports.default = OfferSelector;