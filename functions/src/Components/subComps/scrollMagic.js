'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var ScrollMagic = require('ScrollMagic');
require('animation.gsap');
require('debug.addIndicators');
var TimelineMax = require('TimelineMax');

var scrollMagic = exports.scrollMagic = function scrollMagic(className) {
  var controller = new ScrollMagic.Controller({
    globalSceneOptions: {
      triggerHook: 'onLeave',
      offset: 0
    } });
  var slideParallaxScene = new ScrollMagic.Scene({
    triggerElement: '.topPage',
    triggerHook: 1,
    duration: "100%"
  }).setTween(TweenMax.to(className, 1, {
    y: '50%',
    ease: Power0.easeNone
  })).addTo(controller);
};