'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _getLinkFromAddressToDb = require('./getLinkFromAddressToDb.js');

var _MediaQueriesImgIndex = require('./MediaQueriesImgIndex.js');

var _MediaQueriesImgIndex2 = _interopRequireDefault(_MediaQueriesImgIndex);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var link = "someTile";
var compliedClass;

var Offer = function (_React$Component) {
  _inherits(Offer, _React$Component);

  function Offer(props) {
    _classCallCheck(this, Offer);

    var _this = _possibleConstructorReturn(this, (Offer.__proto__ || Object.getPrototypeOf(Offer)).call(this, props));

    _this.handleClick = _this.handleClick.bind(_this);
    return _this;
  }

  _createClass(Offer, [{
    key: 'handleClick',
    value: function handleClick() {
      console.log((0, _getLinkFromAddressToDb.LinkToDb)(this.props.story.title));
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      console.log("this.props.story");
      console.log(this.props.story);

      compliedClass = "offer " + this.props.story.addedClass;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: compliedClass, onClick: this.handleClick },
          _react2.default.createElement(_MediaQueriesImgIndex2.default, { story: this.props.story }),
          _react2.default.createElement(
            'div',
            { className: 'offer__title' },
            ' ',
            this.props.story.title,
            ' '
          )
        )
      );
    }
  }]);

  return Offer;
}(_react2.default.Component);

exports.default = Offer;