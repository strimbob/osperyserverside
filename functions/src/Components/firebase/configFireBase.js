"use strict";

var _firebase = require("firebase");

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var config = {
  apiKey: "AIzaSyBpeyhLD8N9HDEFCqaWjwMrUjVboHnNWUA",
  authDomain: "osprey-1bf12.firebaseapp.com",
  databaseURL: "https://osprey-1bf12.firebaseio.com",
  projectId: "osprey-1bf12",
  storageBucket: "osprey-1bf12.appspot.com",
  messagingSenderId: "698621013405"
};
_firebase2.default.initializeApp(config);