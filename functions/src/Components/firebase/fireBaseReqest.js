'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStoryFireBaseLimited = exports.sendStoryLinkFireBase = exports.getStoryFireBase = undefined;

var _firebase = require('firebase');

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getStoryFireBase = exports.getStoryFireBase = function getStoryFireBase(string) {
  var ref = _firebase2.default.database().ref(string);
  return ref.once('value').then(function (snap) {
    return snap.val();
  });
};

var sendStoryLinkFireBase = exports.sendStoryLinkFireBase = function sendStoryLinkFireBase(userId, _id, bool) {
  _firebase2.default.database().ref('storys/' + userId).set({ id: _id, isConplete: bool });
};

var getStoryFireBaseLimited = exports.getStoryFireBaseLimited = function getStoryFireBaseLimited(string, limit) {
  var ref = _firebase2.default.database().ref(string);
  return ref.orderByKey().limitToFirst(limit).once('value').then(function (snap) {
    return snap.val();
  });
};