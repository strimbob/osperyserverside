'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _offer = require('./subComps/offer.js');

var _offer2 = _interopRequireDefault(_offer);

var _menuBar = require('./subComps/menuBar.js');

var _menuBar2 = _interopRequireDefault(_menuBar);

var _splatOfferPage = require('./subComps/splatOfferPage.js');

var _splatOfferPage2 = _interopRequireDefault(_splatOfferPage);

var _offerSelector = require('./subComps/offerSelector.js');

var _offerSelector2 = _interopRequireDefault(_offerSelector);

var _shareButtons = require('./subComps/shareButtons.js');

var _shareButtons2 = _interopRequireDefault(_shareButtons);

var _fireBaseReqest = require('./firebase/fireBaseReqest.js');

var _getLinkFromAddressToDb = require('./subComps/getLinkFromAddressToDb.js');

var _scrollMagic = require('./subComps/scrollMagic.js');

var _iframes = require('./subComps/iframes.js');

var _iframes2 = _interopRequireDefault(_iframes);

var _footer = require('./subComps/footer.js');

var _footer2 = _interopRequireDefault(_footer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var OfferPage = function (_React$Component) {
  _inherits(OfferPage, _React$Component);

  function OfferPage(props) {
    _classCallCheck(this, OfferPage);

    var _this = _possibleConstructorReturn(this, (OfferPage.__proto__ || Object.getPrototypeOf(OfferPage)).call(this, props));

    _this.state = {
      storyArray: [],
      storyArrayMain: [],
      iframes: [],
      images: []
    };
    return _this;
  }

  _createClass(OfferPage, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _this2 = this;

      (0, _fireBaseReqest.getStoryFireBase)((0, _getLinkFromAddressToDb.getDbLinkToContent)(this.props)).then(function (_story) {
        var iframes = [];
        if (_story.iframes != null) {
          for (var q = 0; q < _story.iframes.length; q++) {
            iframes.push(_story.iframes[q]);
          }
        }
        var images = [];
        console.log('_story.numberImage.length');
        console.log(_story.numberImage);
        for (var a = 1; a < _story.numberImage + 1; a++) {
          var img = _story.imgPhone.split('0')[0];
          images.push(img + a + '.jpg');
          console.log(img + a);
        }

        _this2.setState({ storyArrayMain: _story,
          iframes: iframes,
          images: images });
      });

      (0, _fireBaseReqest.getStoryFireBase)('offers').then(function (_story) {
        var allStory = [];
        for (var q = 0; q < 3; q++) {
          allStory.push(_story[Object.keys(_story)[q]]);
          allStory[q].addedClass = "offer__threeUp";
        }
        _this2.setState({ storyArray: allStory });
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      (0, _scrollMagic.scrollMagic)('.splat');
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_menuBar2.default, null),
        _react2.default.createElement(_splatOfferPage2.default, { data: this.state.storyArrayMain }),
        _react2.default.createElement(
          'div',
          { className: 'content' },
          _react2.default.createElement(
            'div',
            { className: 'content__box' },
            _react2.default.createElement(
              'div',
              { className: 'content__title' },
              this.state.storyArrayMain.title
            ),
            _react2.default.createElement(
              'div',
              { className: 'content__shareButtons' },
              _react2.default.createElement(_shareButtons2.default, { data: this.props, title: this.state.storyArrayMain.title })
            ),
            _react2.default.createElement('div', { className: 'content__line' }),
            _react2.default.createElement(
              'div',
              { className: 'content__body' },
              this.state.storyArrayMain.textBody
            ),
            _react2.default.createElement('img', { className: 'content_img', src: this.state.storyArrayMain.pageImage, alt: '\\|/' }),
            _react2.default.createElement('div', { className: 'content__line' }),
            _react2.default.createElement(
              'div',
              { className: 'content__Extras' },
              this.state.iframes.map(function (iframes, index) {
                return _react2.default.createElement(_iframes2.default, { data: iframes, key: index });
              })
            ),
            _react2.default.createElement(
              'div',
              { className: 'content__Extras' },
              this.state.images.map(function (images, index) {
                {
                  console.log(images);
                }
                return _react2.default.createElement('img', { className: 'content__iframes', src: images, alt: '\\/\\', key: index });
              })
            ),
            _react2.default.createElement(
              'div',
              { className: 'content__bottom' },
              _react2.default.createElement(
                'div',
                { className: 'content__titleBottom content__title--sub' },
                'other work'
              ),
              _react2.default.createElement(
                'div',
                { className: 'content__subOffer' },
                this.state.storyArray.map(function (storyArray, index) {
                  return _react2.default.createElement(_offer2.default, { story: storyArray, key: index });
                })
              ),
              _react2.default.createElement(_footer2.default, null)
            )
          )
        )
      );
    }
  }]);

  return OfferPage;
}(_react2.default.Component);

exports.default = OfferPage;