'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ssrapp = undefined;

var _firebaseFunctions = require('firebase-functions');

var functions = _interopRequireWildcard(_firebaseFunctions);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _server = require('react-dom/server');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _getLinkFromAddressToDb = require('./src/Components/subComps/getLinkFromAddressToDb.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var firebase = require('firebase-admin');
var firebaseApp = firebase.initializeApp(functions.config().firebase);
// import App from './src/app';

var getStory = require('./src/backend/getOffer.js');
// import Contentsmainpage from './src/contentsMainPage.js'

// import {getStory} from './src/backend/getOffer.js';

//use the a sync version !!!!!!!!!!!! not readFileSync
var index = _fs2.default.readFileSync(__dirname + '/index.html', 'utf8');

var app = (0, _express2.default)();

var offeslink = [];
offeslink.push("https://docs.google.com/document/d/1tbtqQl44hAI_eiWEWDFPT5iMXj0PuNgzHQs52RNG-VA/edit");
offeslink.push("https://docs.google.com/document/d/1bVLjJroSMcjh6bu71XvsEzfrUIbIHtcZkgoS3bxdHqc/edit");
offeslink.push("https://docs.google.com/document/d/1tbtqQl44hAI_eiWEWDFPT5iMXj0PuNgzHQs52RNG-VA/edit");
offeslink.push("https://docs.google.com/document/d/1uYeI3IhI1xInj0dWVGnbVnmmE4MxX2Rqun8wV1iZiEE/edit");
offeslink.push("https://docs.google.com/document/d/1Rqhgn4yeTc8zhGf7pr8pGUuWgO2FpcoxjbZjtjEvSGw/edit");
offeslink.push("https://docs.google.com/document/d/1YulFuEbprabP6aWLuFZStXWNy2U0OvJwlaSCQwxXthg/edit");
offeslink.push("https://docs.google.com/document/d/1jwuEA3TpsxfUbgvYpqv2rL1ugKxru1mMN9NpZGM_TME/edit");
offeslink.push("https://docs.google.com/document/d/1H4RcpkHbimuKPfT3xNkL5PdWSmBhvTZ0XSuxv3qE_Jk/edit");

app.get('/update', function (req, res) {

  for (var q = 0; q < offeslink.length; q++) {
    var link = (0, _getLinkFromAddressToDb.getLinkToGoogleDocs)(offeslink[q]);
    getStory.getStory(link, firebase);
  }
  res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
  res.send('finalHtmlsadf');
});

// app.get('/offers/**',(req,res)=>{
//   const html =   renderToString(<Contentsmainpage />);
//   const indexs = fs.readFileSync(__dirname + '/index.html', 'utf8');
//   const finalHtml = indexs.replace('<!-- ::APP:: -->',html);
//   res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
//   res.send(html);
// });

// app.get('**',(req,res)=>{
//   const html =   renderToString(<App />);
//   const finalHtml = index.replace('<!-- ::APP:: -->',html);
//   res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
//   res.send(finalHtml);
// });

var ssrapp = exports.ssrapp = functions.https.onRequest(app);