# ben's website template ....
## How to install and use  the files?

## install the client files
1. Clone this repo `git clone https://strimbob@bitbucket.org/strimbob/osperyserverside.git `
2. Run `npm install` to install all the dependencies
3. Run `npm start` for development mode
4. New browser window should open automatically on localhost:8080.
5. Run `npm run prod` to make the website

## install the server files.
1. cd functions
2. npm install
3. cd ..

## update the git
1. git add .
2. git commit -m 'what is differnet'
3. git push origin master

## get changes the git
1.  git pull origin master

## to install the backend run This
1. npm install -g firebase-tools

## add to a firebase project
1. node_modules/.bin/firebase use --add

## compile the client folder
1. npm run webpack

## compile the server and web
1. npm run babel

## local testing for the web app
1. node_modules/.bin/firebase serve --only  functions,hosting

## deploy the website in full
1. node_modules/.bin/firebase deploy
