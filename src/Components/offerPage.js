import React from 'react';
import Offer from './subComps/offer.js'
import MenuBar from './subComps/menuBar.js'
import SplatOfferPage from './subComps/splatOfferPage.js'
import OfferSelector from './subComps/offerSelector.js'
import Share from './subComps/shareButtons.js'
import {getStoryFireBase} from './firebase/fireBaseReqest.js'
import {contentFromDb} from './subComps/getLinkFromAddressToDb.js'
import {getDbLinkToContent} from './subComps/getLinkFromAddressToDb.js'
import {getLinkfromWindow} from './subComps/getLinkFromAddressToDb.js'
import {scrollMagic} from './subComps/scrollMagic.js'
import Iframes from './subComps/iframes.js'
import Footer from './subComps/footer.js'

export default class OfferPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      storyArray: [],
      storyArrayMain: [],
      iframes:[],
      images:[]
    };
  }

  componentWillMount() {

    getStoryFireBase( getDbLinkToContent(this.props)).then(_story => {
var iframes = []
if(_story.iframes != null){
      for (var q = 0; q < _story.iframes.length; q++) {
           iframes.push (_story.iframes[q]);
      }
}
var images = []
console.log('_story.numberImage.length');
console.log(_story.numberImage);
for(var a =  1;a<_story.numberImage+1;a++){
  let img = _story.imgPhone.split('0')[0];
  images.push(img+a+'.jpg')
console.log(img+a);
}

      this.setState({storyArrayMain: _story,
                    iframes: iframes,
                  images: images})

    });

    getStoryFireBase('offers').then(_story => {
      var allStory = [];
      for (var q = 0; q < 3; q++) {
        allStory.push(_story[Object.keys(_story)[q]]);
        allStory[q].addedClass = "offer__threeUp";
      }
      this.setState({storyArray: allStory})
    });
  }

  componentDidMount() {
    scrollMagic('.splat')
  }

  render() {
    return (<div>
      <MenuBar/>
      <SplatOfferPage data={this.state.storyArrayMain}/>
      <div className='content'>
        <div className='content__box'>

          <div className='content__title'>{this.state.storyArrayMain.title}</div>
          <div className='content__shareButtons'>
            <Share data={this.props} title={this.state.storyArrayMain.title  }/>
          </div>
          <div className='content__line'></div>
          <div className='content__body'>{this.state.storyArrayMain.textBody}
          </div>
          <img className='content_img' src={this.state.storyArrayMain.pageImage} alt='\|/'></img>
          <div className='content__line'></div>
            <div className='content__Extras'>
              {this.state.iframes.map(function(iframes, index) {
                return  <Iframes data={iframes} key={index}/>})}
                </div>

  <div className='content__Extras'>
    {this.state.images.map(function(images, index) {
      {console.log(images)}
      return  <img className='content__iframes' src={images} alt='\/\'  key={index}/>})}
      </div>


          <div className='content__bottom'>
          <div className='content__titleBottom content__title--sub'>other work</div>
          <div className='content__subOffer'>
            {
              this.state.storyArray.map(function(storyArray, index) {
                return <Offer story={storyArray} key={index}/>
              })
            }
            </div>
            <Footer />
          </div>
        </div>
      </div>
    </div>);
  }
}
