import firebase from 'firebase'

export const getStoryFireBase = (string)=>{
  const ref = firebase.database().ref(string);
  return ref.once('value').then(snap => snap.val());
}

export const sendStoryLinkFireBase = (userId, _id, bool) =>{
  firebase.database().ref('storys/' + userId).set({id: _id, isConplete: bool});
}

export const getStoryFireBaseLimited = (string, limit)=>{
  const ref = firebase.database().ref(string);
  return ref.orderByKey().limitToFirst(limit).once('value').then(snap => snap.val());
}
