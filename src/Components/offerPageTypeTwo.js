import React from 'react';

import Offer from './subComps/offer.js'
import MenuBar from './subComps/menuBar.js'
import Splat from './subComps/splat.js'
import OfferSelector from './subComps/offerSelector.js'
import Share from './subComps/shareButtons.js'
import {getStoryFireBase} from './firebase/fireBaseReqest.js'


export default class OfferPageTypeTwo extends React.Component {

  constructor(props) {
  super(props);
  this.state = {storyArray: []};
}

  componentWillMount() {
    getStoryFireBase('offers').then(_story => {
      var allStory = [];
      for (var q = 0; q < 3; q++) {
        console.log(_story[Object.keys(_story)[q]]);
        allStory.push(_story[Object.keys(_story)[q]]);
      }
      this.setState({storyArray: allStory})
    });
  }

  componentDidMount() {

  }

  render() {

    return (<div>
      <MenuBar/>
      <div className='content'>
        <div className='content__box'>
          <div className='content__title'>360˚ EXPERIENCE</div>
          <div className='content__line'></div>
            <div className='content__space'></div>
            <img className='content__Image' src='http://lorempixel.com/250/250/sports/'alt='/|\' ></img>
          <div className='content__body'>
            360˚ experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods… you get the picture… experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360˚ experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods… you get the picture… experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360˚ experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods… you get the picture… experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360˚ experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods… you get the picture… experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas 360˚ experiences exhibit your project in a way that has never previously been possible. the interactive and zoomable panoramas give effective and practical context to large areas, communicating multiple levels of detail to your viewers. information hotspots display information, pictures and video, and can include links to external websites, documents and images. multiple layers allow curious viewers to dive into other panoramas, showing the inside of the building they chose, or the garden, or the woods… you get the picture… experience. particularly useful for country estates, rural planning, parks, national parks, country parks, theme parks, urban areas, heritage sites, camp grounds, gardens, conservation areas; the list is endless. contact us to discuss your ideas
          </div>
          <div className='content__line'></div>
          <div className='content__shareButtons'>
            <Share/>
          </div>
          <div className='content__line'></div>
          <div className='content__title content__title--sub'>other work</div>
          <div className='content__subOffer'>

            {this.state.storyArray.map(function(storyArray, index) {
              return <Offer  story={storyArray}  key={index}/>})}

          </div>
        </div>
      </div>
    </div>);
  }
}

// require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicator');
// import  addIndicators from 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
// import ScrollMagic from 'scrollmagic';
// import { TweenMax } from 'gsap';
// import * as ScrollMagic from 'ScrollMagic';
// import "ScrollMagic/scrollmagic/minified/plugins/debug.addIndicators.min.js";
// import 'ScrollMagic/scrollmagic/minified/plugins/animation.gsap.min.js';
