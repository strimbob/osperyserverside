import React from 'react';
import Offer from './subComps/offer.js'
import MenuBar from './subComps/menuBar.js'
// import Splat from './subComps/splat.js'
import OfferSelector from './subComps/offerSelector.js'
export default class App extends React.Component {

  render() {
    return (
      <div>
      <MenuBar />
      <OfferSelector />
      </div>
    );
  }
}
