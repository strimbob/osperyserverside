import React from 'react';
import {ShareButtons,ShareCounts,generateShareIcon} from 'react-share';

const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  PinterestShareButton,
  VKShareButton,
  OKShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  RedditShareButton,
  EmailShareButton,
} = ShareButtons;

const {
  FacebookShareCount,
  GooglePlusShareCount,
  LinkedinShareCount,
  PinterestShareCount,
  VKShareCount,
  OKShareCount,
  RedditShareCount,
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');
const PinterestIcon = generateShareIcon('pinterest');
const VKIcon = generateShareIcon('vk');
const OKIcon = generateShareIcon('ok');
const TelegramIcon = generateShareIcon('telegram');
const WhatsappIcon = generateShareIcon('whatsapp');
const RedditIcon = generateShareIcon('reddit');
const EmailIcon = generateShareIcon('email');

export default class Share  extends React.Component{
  render() {



  let dbAddress = this.props.data.location.pathname;
  let dbSearch = this.props.data.location.search;

  // let dbAddress = "this.props.data.location.pathname";
  // let dbSearch = "this.props.data.location.search";

  console.log("dbAddress+dbSearch");
  console.log(dbAddress+dbSearch);
  let address = 'http://osprey.co.uk'+dbAddress+dbSearch;
  console.log( this.props.data);
  const shareUrl = address;
  const title =  this.props.title;


    // const shareUrl = 'sfsdfs';
    // const title =  'gdsf';

  return (
    <div className="Demo__container">
      <div className="Demo__some-network">
        <FacebookShareButton
          url={shareUrl}
          quote={title}
          className="Demo__some-network__share-button">
          <FacebookIcon
            size={32}
            round />
        </FacebookShareButton>

      </div>

      <div className="Demo__some-network">
        <TwitterShareButton
          url={shareUrl}
          title={title}
          className="Demo__some-network__share-button">
          <TwitterIcon
            size={32}
            round />
        </TwitterShareButton>

        <div className="Demo__some-network__share-count">
          &nbsp;
        </div>
      </div>

      <div className="Demo__some-network">
        <TelegramShareButton
          url={shareUrl}
          title={title}
          className="Demo__some-network__share-button">
          <TelegramIcon size={32} round />
        </TelegramShareButton>

        <div className="Demo__some-network__share-count">
          &nbsp;
        </div>
      </div>

      <div className="Demo__some-network">
        <WhatsappShareButton
          url={shareUrl}
          title={title}
          separator=":: "
          className="Demo__some-network__share-button">
          <WhatsappIcon size={32} round />
        </WhatsappShareButton>

        <div className="Demo__some-network__share-count">
          &nbsp;
        </div>
      </div>

      <div className="Demo__some-network">
        <GooglePlusShareButton
          url={shareUrl}
          className="Demo__some-network__share-button">
          <GooglePlusIcon
            size={32}
            round />
        </GooglePlusShareButton>

      </div>

      <div className="Demo__some-network">
        <LinkedinShareButton
          url={shareUrl}
          title={title}
          windowWidth={750}
          windowHeight={600}
          className="Demo__some-network__share-button">
          <LinkedinIcon
            size={32}
            round />
        </LinkedinShareButton>
      </div>
      <div className="Demo__some-network">
        <RedditShareButton
          url={shareUrl}
          title={title}
          windowWidth={660}
          windowHeight={460}
          className="Demo__some-network__share-button">
          <RedditIcon
            size={32}
            round />
        </RedditShareButton>
      </div>
      <div className="Demo__some-network">
        <EmailShareButton
          url={shareUrl}
          subject={title}
          body="body"
          className="Demo__some-network__share-button">
          <EmailIcon
            size={32}
            round />
        </EmailShareButton>
      </div>
    </div>
  );
      }
    }
