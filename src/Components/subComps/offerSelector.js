import React from 'react';
import Offer from './offer.js'
import Divider from './divder.js'
import {getStoryFireBase} from '../firebase/fireBaseReqest.js'

var counterL = 0;
var counterR = 0;
var switcherL = 0;
var switcherR = 1;
export default class OfferSelector extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        storyArray: []
      };
    }

    componentWillMount() {
      var lic = require('../../../public/static/offer/360˚ EXPERIENCE.js')
      console.log(lic);
      getStoryFireBase('offers').then(_story => {
        var allStory = [];
        for (var q = 0; q < Object.keys(_story).length; q++) {
          allStory.push(_story[Object.keys(_story)[q]]);
          allStory[q].length = Object.keys(_story).length;
        }
        this.setState({storyArray: allStory})
      });
    }

    render() {
        return ( <  div >
          <div className = 'offerSelector' > {
            this.state.storyArray.map(function(storyArray, index) {
                  addLeftRight(index);
                  function addLeftRight(index) {
                    let whichSide = (index & 1) ? 'right' : 'left';
                    storyArray.addedClass = "offer__" + whichSide;
                    if (whichSide == 'left') {
                      bigSmall(switcherL);
                      switcherL = !switcherL;
                    } else {
                      bigSmall(switcherR);
                      switcherR = !switcherR;
                    }
                  }
                  function bigSmall(_switcher) {
                    if (_switcher) storyArray.indexDisplay = 'big';
                    else storyArray.indexDisplay = 'small';
                    storyArray.addedClass += ' ' + 'offer__' + storyArray.indexDisplay;
                  }
                  return <Offer story = {storyArray}key = {index}/>})}
                  </div>
                  </div>
                );
              }
            }
