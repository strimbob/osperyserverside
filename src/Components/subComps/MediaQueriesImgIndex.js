import React from 'react';
import MediaQuery from 'react-responsive';
import media from '../../scss/mediaRect.js';

var ImgDestop;
var ImgPhone;

export default class MediaQueriesImgIndex  extends React.Component{

  constructor(props) {
  super(props);
  this.state = {ImgDestop:  this.props.story.imgDtop,
  ImgPhone : this.props.story.imgPhone
};
}


  componentDidMount() {

if(this.props.story.indexDisplay =='big'){
this.setState({
ImgDestop : this.props.story.imgDtop,
ImgPhone : this.props.story.imgPhone
})
// this.setState({
// ImgDestop : "http://via.placeholder.com/400x230",
// ImgPhone : "http://via.placeholder.com/400x230"
// })


}else if (this.props.story.indexDisplay =='small'){
  this.setState({
  ImgDestop : this.props.story.imgDtopS,
   ImgPhone  : this.props.story.imgPhoneS
 })

//  this.setState({
//   ImgDestop : "http://via.placeholder.com/217x126",
//   ImgPhone  : "http://via.placeholder.com/269x400"
// })

}
  }

  render(){
    return(
    <div>
      <MediaQuery minWidth={media.desktop+1}>
        <img  src= { this.state.ImgDestop} alt='\|/' />
      </MediaQuery>
      <MediaQuery  maxWidth={media.desktop}>
        <img src= {this.state.ImgPhone} alt='\|/' />
      </MediaQuery>
    </div>
        );
      }
    }
