import React from 'react';
export default class Footer extends React.Component {
  render() {
    return (
      <div>
        <div className='footer' >
<img className='footer__logo' src='./static/images/logos/white40x40.png' alt='\|/'></img>
<div className='footer__title' > OSPREY - no fixed geographical location  </div>
        </div>
      </div>
    );
  }
}
