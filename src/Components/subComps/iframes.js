import React from 'react';
import Iframe from 'react-iframe'
export default class Iframes extends React.Component {
  constructor(props) {
  super(props);
}

  render() {
    return (
          <iframe className='content__iframes' src={this.props.data} ></iframe>
    );
  }
}
