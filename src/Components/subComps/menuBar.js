import React from 'react';
export default class MenuBar extends React.Component {
  render() {
    return (
      <div>

        <div className='menuBar' >

<img className='menuBar__logo' src='./static/images/logos/white40x40.png' alt='\|/'></img>
<div className='menuBar__title' > OSPREY IMAGERS </div>
        </div>
                <div className='topPage'></div>
      </div>
    );
  }
}
