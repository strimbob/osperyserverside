import React from 'react';
import {LinkToDb} from './getLinkFromAddressToDb.js'
import MediaQueriesImgIndex from './MediaQueriesImgIndex.js';
var link = "someTile";
var compliedClass;
export default class Offer extends React.Component {

  constructor(props) {
  super(props);
  this.handleClick = this.handleClick.bind(this);
}
handleClick() {
  console.log(LinkToDb(this.props.story.title));

}
componentWillMount(){
  console.log("this.props.story");
  console.log(this.props.story);

   compliedClass = "offer " + this.props.story.addedClass;
}

  render() {
    return (
      <div>
        <div className={compliedClass} onClick={this.handleClick}>
        <MediaQueriesImgIndex story={this.props.story}/>
        <div className='offer__title' > {this.props.story.title} </div>
        </div>
      </div>
    );
  }
}
