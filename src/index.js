import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter , Switch, Router, Route ,Redirect} from 'react-router-dom'
import App from './App';
import OfferPage from './Components/OfferPage.js'
import OfferPageTypeTwo from './Components/offerPageTypeTwo.js'
import config from './Components/firebase/configFireBase.js'
require('./scss/screen.scss');

render((
    <BrowserRouter basename={process.env.PUBLIC_URL}>

    <Switch>
      <Route exact path="/"       component={App} />
      <Route path='/someTile' render={props => <OfferPage {...props}  /> } />
      <Route path='/someTileTwo' render={props => <OfferPageTypeTwo {...props}  /> } />
      <Route path='/offers' render={props => <OfferPage {...props}  /> } />
      <Route exact path='/workplease'   render={props => <OfferPage {...props}  /> }   />
    </Switch>
    </BrowserRouter>
),document.querySelector('#root'));



    // render(<App />, document.querySelector('#root'));
