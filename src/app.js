import React from 'react';
import Offer from './Components/subComps/offer.js'
import MenuBar from './Components/subComps/menuBar.js'
import Splat from './Components/subComps/splat.js'
import Footer from './Components/subComps/footer.js'
import OfferSelector from './Components/subComps/offerSelector.js'
function App(props) {

  return(
    <div>
    <MenuBar />
      <Splat />
    <OfferSelector />
    <Footer />
    </div>
  );
}
export default App;
